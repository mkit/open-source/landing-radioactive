require('dotenv').config();
const path = require('path');

const {
  SITE_URL,
  SITE_TITLE,
  SITE_TITLE_SHORT,
  SITE_DESCRIPTION,
  SITE_KEYWORDS,
  SITE_THEME_COLOR,
  SITE_BACKGROUND_COLOR,
  SITE_LOGO_RAW,
  SITE_SOCIAL_IMAGE_RAW,
  TWITTER_ACCOUNT_ID,
  FACEBOOK_APP_ID,
  FACEBOOK_PIXEL_ID,
  GOOGLE_ANALYTICS_ID,
  MAILCHIMP_ENDPOINT
} = process.env;

module.exports = {
  siteMetadata: {
    siteUrl: SITE_URL, // required by gatsby-plugin-robots-txt
    siteTitle: SITE_TITLE,
    siteTitleShort: SITE_TITLE_SHORT,
    siteDescription: SITE_DESCRIPTION,
    siteKeywords: SITE_KEYWORDS,
    siteThemeColor: SITE_THEME_COLOR,
    siteSocialImageRaw: path.resolve(__dirname, SITE_SOCIAL_IMAGE_RAW),
    twitterAccountId: TWITTER_ACCOUNT_ID,
    facebookAppId: FACEBOOK_APP_ID
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-material-ui',
      options: {}
    },
    {
      resolve: `gatsby-plugin-nprogress`,
      options: {
        color: SITE_THEME_COLOR
      }
    },
    {
      resolve: 'gatsby-plugin-google-analytics',
      options: {
        trackingId: GOOGLE_ANALYTICS_ID,
        head: true
      }
    },
    {
      resolve: `gatsby-plugin-facebook-pixel`,
      options: {
        pixelId: FACEBOOK_PIXEL_ID
      }
    },
    {
      resolve: 'gatsby-plugin-mailchimp',
      options: {
        endpoint: MAILCHIMP_ENDPOINT
      }
    },
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: SITE_TITLE,
        short_name: SITE_TITLE_SHORT,
        start_url: '/',
        theme_color: SITE_THEME_COLOR,
        background_color: SITE_BACKGROUND_COLOR,
        display: 'minimal-ui',
        icon: path.resolve(__dirname, SITE_LOGO_RAW)
      }
    },
    {
      resolve: 'gatsby-plugin-root-import',
      options: {
        '~src': path.resolve(__dirname, 'src/'),
        '~pages': path.resolve(__dirname, 'src/pages/'),
        '~layout': path.resolve(__dirname, 'src/layout/'),
        '~containers': path.resolve(__dirname, 'src/containers/'),
        '~components': path.resolve(__dirname, 'src/components/')
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: path.resolve(__dirname, 'src/assets/images')
      }
    },
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-robots-txt',
    'gatsby-plugin-sitemap',
    'gatsby-plugin-offline',
    'gatsby-plugin-webpack-size'
  ]
};
