# :blue_book: mkit-bundle-gatsby

> Gatsby project starter

**Improvements, bug reports and PRs are more than welcome!**

## Features

### Project

- [x] :rocket: **Latest JS support**
- [x] :gem: **ReactJS & PropTypes**
- [x] :zap: **ESLint, Prettier, EditorConfig**
- [x] :wrench: **`.env` configuration**
- [x] :open_file_folder: **Clean folder structure**
- [x] :construction_worker: **`npm run` or `yarn` all the things**
- [x] :seedling: **[Hygen](http://www.hygen.io/) for quick and consistent file creation**
- [x] :vertical_traffic_light: **Pre-commit hooks**
- [x] :100: **[Google Lighthouse 100/100 score](https://developers.google.com/web/tools/lighthouse/)**
- [x] :rocket: **Production ready**

### Plugins

- [x] :crystal_ball: **[Material UI](https://www.gatsbyjs.org/packages/gatsby-plugin-material-ui)**
- [x] :recycle: **[NProgress](https://www.gatsbyjs.org/packages/gatsby-plugin-nprogress)**
- [x] :chart_with_upwards_trend: **[Google Analytics](https://www.gatsbyjs.org/packages/gatsby-plugin-google-analytics/)**
- [x] :chart_with_upwards_trend: **[Facebook Pixel](https://www.gatsbyjs.org/packages/gatsby-plugin-facebook-pixel)**
- [x] :mailbox: **[MailChimp](https://www.gatsbyjs.org/packages/gatsby-plugin-mailchimp)**
- [x] :scroll: **[Manifest](https://www.gatsbyjs.org/packages/gatsby-plugin-manifest)**
- [x] :link: **[Helmet](https://www.gatsbyjs.org/packages/gatsby-plugin-react-helmet)**
- [x] :robot: **[Robots.txt](https://www.gatsbyjs.org/packages/gatsby-plugin-robots-txt)**
- [x] :globe_with_meridians: **[Sitemap](https://www.gatsbyjs.org/packages/gatsby-plugin-sitemap)**
- [x] :airplane: **[Offline](https://www.gatsbyjs.org/packages/gatsby-plugin-offline)**
- [x] :point_right: **[Webpack Root Import](https://www.gatsbyjs.org/packages/gatsby-plugin-root-import)**
- [x] :elephant: **[Webpack Size](https://www.gatsbyjs.org/packages/gatsby-plugin-webpack-size/?=webpack-size)**

### Components

- [x] :herb: **Configurable MUI theme**
- [x] :cyclone: **High-level `Page` wrapper component**
- [x] :mag_right: **`Seo` component with `json-ld` data**
- [x] :triangular_ruler: **`Container` component**
- [x] :star: **`Link` component**

## Getting Started

### 1. Clone the repository

```bash
$ git clone https://github.com/mkitio/mkit-bundle-gatsby.git <your-project-name>
```

### 2. Make it your own

```bash
$ cd <your-project-name> && rm -rf .git && git init
```

### 3. Install the dependencies

```bash
$ yarn
```

### 4. Secrets and variables

#### 4.1 Adapt all secrets to your own values.

```bash
$ cp .env.example .env
```

#### 4.2 Adapt all variables and plugins to your liking.

```bash
$ nano gatsby-config.js
# Or
$ code gatsby-config.js
```

#### 4.3 Change JSON-LD values in `src/layout/Seo.js`

```bash
$ nano src/layout/Seo.js
# Or
$ code src/layout/Seo.js
```

#### 4.4 Replace images

```bash
# Image used for favicon and manifest icons
$ static/images/site_logo_raw.png
# SEO and social media image
$ static/images/site_social_image_raw.png
```

### 5. Test your installation with production build

```bash
$ yarn build && yarn serve
```

### 6. Develop

```bash
$ yarn develop
```

## Folder Structure

### Path Aliases

The project uses [Webpack's `resolve.alias`](https://webpack.js.org/configuration/resolve/#resolvealias) to support import aliases.

```bash
# Path aliases can be found in `gatsby-config.js` and `jsconfig.json`
~src --> src/
~pages --> src/pages/
~layout --> src/layout/
~containers --> src/containers/
~components --> src/components/
```

```js
// You can do...
import MyComponent from '~components/MyComponent';
// Instead of...
import MyComponent from '../../../components/MyComponent';
```

### Tree View

```bash
.
├── .hygen                                # Hygen file templates
├── .vscode                               # VSCode workspace config
│   ├── extensions.json                     # Recommended extensions
│   └── settings.json                       # Workspace settings
├── src                                   # Source code
│   ├── components                          # Components
│   │   ├── Container.js                      # Section container
│   │   └── Link.js                           # Reusable Gatsby Link wrapper
│   ├── containers                          # Containers
│   ├── layout                              # High-level components
│   │   ├── Page.js                           # Page wrapper (Layout)
│   │   └── Seo.js                            # SEO
│   ├── pages                               # Pages
│   │   ├── 404.js                            # Customizable 404 page
│   │   └── index.js                          # Home page
│   ├── App.js                              # High-level app wrapper
│   └── theme.js                            # MUI theme configuration
├── static                                # Statically served files
├── .gitignore                            # Ignored files by Git
├── .eslintrc                             # ESLint config
├── .prettierrc                           # Prettier config
├── .editorconfig                         # EditorConfig
├── .env.example                          # dotenv secrets
├── gatsby-config.js                      # Gatsby global config
├── gatsby-ssr.js                         # Gatsby SSR config
├── gatsby-browser.js                     # Gatsby CSR config
├── jsconfig.json                         # VSCode JS-specific config
├── README.md
├── package.json
└── yarn.lock
```

## Available Scripts

In the project directory, you can run:

```bash
# Local development
$ yarn develop

# Production build
$ yarn build

# Serve production build
$ yarn serve
```

In addition, the following scripts can be used to quickly scaffold `components/`, `containers/`, `layout/`, or `page/` files.

```bash
# Templates live under `.hygen/`
$ yarn gen:component  [component-name]
$ yarn gen:container  [container-name]
$ yarn gen:layout     [layout-name]
$ yarn gen:page       [page-name]
```

## Deploy

Running `yarn build` outputs a ready-to-use production build of your project to `public/`. This folder can be statically served by any CDN or webserver such as [NGINX](https://www.nginx.com/).

Make sure you do `yarn build && yarn serve` to preview your changes before deploying live.

## Authors

**Stoyan Merdzhanov** - Initial work - [MK IT](https://mkit.io)

See also the list of [contributors](https://github.com/mkitio/mkit-bundle-gatsby/contributors) who participated in this project.

## Acknowledgments

A hat-tip to Fabian Schultz ([@fschultz\_](https://twitter.com/fschultz_))! This project was heavily inspired by his amazing starter [`gatsby-universal`](https://github.com/fabe/gatsby-universal) and hard work!

## License

```
The MIT License (MIT)

Copyright (c) 2018-present MK IT Ltd. <hi@mkit.io>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
