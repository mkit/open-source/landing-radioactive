import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import SEO from '~layout/SEO';
import Navbar from '~layout/Navbar';
import Footer from '~layout/Footer';

const useStyles = makeStyles(theme => ({
  calcMinHeight: {
    minHeight: '100vh',
    marginTop: '-48px',
    '& > div:first-child': {
      marginTop: '48px'
    },
    '& > div:not(:first-child):not(:last-child)': {
      marginTop: props => (props.isMobile ? theme.spacing(6) : ''),
      marginBottom: props => (props.isMobile ? theme.spacing(6) : '')
    },
    '& > div:nth-child(n+5)': {
      marginTop: props => (props.isMobile ? '' : theme.spacing(6))
    }
  }
}));

const Page = ({ children }) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.only('xs'));
  const classes = useStyles({ isMobile });
  return (
    <React.Fragment>
      <SEO />

      <Navbar />

      <main className={classes.calcMinHeight}>{children}</main>

      <footer>
        <Footer />
      </footer>
    </React.Fragment>
  );
};

Page.propTypes = {
  children: PropTypes.node.isRequired
};

export default Page;
