import uuid from 'uuid';
import React, { useState, useEffect } from 'react';
import { Link } from 'gatsby';
import { Link as ScrollTo } from 'react-scroll';
import { makeStyles } from '@material-ui/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Tab from '@material-ui/core/Tab';
import Hidden from '@material-ui/core/Hidden';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import Container from '~components/Container';

const useStyles = makeStyles(theme => ({
  appBar: {
    transition: 'background-color 500ms, box-shadow 500ms'
  },
  transparentAppBar: {
    background: 'transparent',
    boxShadow: 'unset',
    transition: 'background-color 500ms, box-shadow 500ms'
  },
  toolbar: {
    width: '100%'
  },
  logoText: {
    marginLeft: theme.spacing(2),
    fontWeight: '400',
    userSelect: 'none',
    '& a': {
      color: 'inherit',
      textDecoration: 'none'
    }
  },
  activeLinkStyle: {
    color: theme.palette.secondary.main,
    borderBottom: `2px solid ${theme.palette.secondary.main}`
  },
  drawer: {
    minWidth: 250
  }
}));

const LINKS = [
  {
    title: 'About',
    href: 'about-0'
  },
  {
    title: 'Pricing',
    href: 'pricing'
  },
  {
    title: 'FAQ',
    href: 'faq'
  },
  {
    title: 'Contacts',
    href: 'contacts'
  }
];

const Navbar = () => {
  const classes = useStyles();
  const [isDrawerOpen, openDrawer] = useState(false);
  const [scrollPosition, setScrollPosition] = useState(0);
  const scrollObserver = () => setScrollPosition(window.scrollY);

  useEffect(() => {
    window.addEventListener('scroll', scrollObserver);

    return () => {
      window.removeEventListener('scroll', scrollObserver);
    };
  }, []);

  return (
    <AppBar
      className={
        scrollPosition === 0 ? classes.transparentAppBar : classes.appBar
      }
    >
      <Container>
        <Toolbar variant="dense" disableGutters className={classes.toolbar}>
          {/* Logo image */}
          <Link to="/" style={{ display: 'flex' }}>
            <img
              src="/images/site_logo_small.png"
              alt="Radio Active"
              className={classes.logoImage}
            />
          </Link>

          {/* Logo text */}
          <Typography variant="h6" component="h2" className={classes.logoText}>
            <Link to="/">RadioActive</Link>
          </Typography>

          {/* Push all to right */}
          <span style={{ flex: 1 }} />

          {/* Links for lgUp */}
          <Hidden mdDown>
            {LINKS &&
              LINKS.map(link => (
                <Tab
                  key={uuid.v4()}
                  component={ScrollTo}
                  label={link.title}
                  to={link.href}
                  smooth
                  spy
                  disableRipple
                  activeClass={classes.activeLinkStyle}
                  classes={{
                    root: classes.tabRoot
                  }}
                />
              ))}
          </Hidden>

          {/* Drawer for mdDown */}
          <Hidden lgUp>
            <IconButton aria-label="Menu" onClick={() => openDrawer(true)}>
              <Icon style={{ color: '#fff' }}>menu</Icon>
            </IconButton>
            <Drawer
              open={isDrawerOpen}
              onClose={() => openDrawer(false)}
              anchor="right"
            >
              <div className={classes.drawer}>
                {LINKS &&
                  LINKS.map(link => (
                    <ListItem
                      key={uuid.v4()}
                      component={ScrollTo}
                      to={link.href}
                      onClick={() => openDrawer(false)}
                      activeClass={classes.activeLinkStyle}
                      smooth
                      button
                      divider
                    >
                      <ListItemText
                        primary={link.title}
                        primaryTypographyProps={{ color: 'inherit' }}
                        inset
                      />
                    </ListItem>
                  ))}
              </div>
            </Drawer>
          </Hidden>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Navbar;
