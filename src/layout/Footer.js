import React from 'react';
import { makeStyles } from '@material-ui/styles';
import useTheme from '@material-ui/core/styles/useTheme';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Container from '~components/Container';
import Link from '~components/Link';

const useStyles = makeStyles(theme => ({
  footer: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    '& h5': {
      color: '#fff',
      '& a': {
        color: 'inherit',
        '&:hover': {
          color: theme.palette.secondary.main
        }
      }
    }
  }
}));

const Footer = () => {
  const theme = useTheme();
  const classes = useStyles();
  const isMobileDevice = useMediaQuery(theme.breakpoints.only('xs'));

  return (
    <Container
      // background={theme.palette.primary.dark}
      background="#212121"
      className={classes.footer}
      center
    >
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        spacing={4}
      >
        <Grid item xs={12} sm>
          <Typography
            variant="body1"
            component="h5"
            align={isMobileDevice ? 'center' : 'left'}
          >
            <Link to="https://mkit.io">Radio Active</Link>
          </Typography>
        </Grid>
        <Grid item xs={12} sm>
          <Typography variant="body1" component="h5" align="center">
            Fugiat irure eiusmod nisi esse.
          </Typography>
        </Grid>
        <Grid item xs={12} sm>
          <Typography
            variant="body1"
            component="h5"
            align={isMobileDevice ? 'center' : 'right'}
          >
            <Link to="https://mkit.io">Design MK IT</Link>
          </Typography>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Footer;
