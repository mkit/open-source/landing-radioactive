import React from 'react';
import makeStyles from '@material-ui/styles/makeStyles';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import { Title } from '~components/Typography';

const inputStyle = createMuiTheme({
  overrides: {
    MuiInput: {
      underline: {
        '&&&&:hover:before': {
          borderBottom: '2px solid #eceff1'
        },
        '&&&&:before': { borderBottom: '1px solid #b0bec5' },
        '&&&&:after': { borderBottom: '1px solid #b0bec5' }
      }
    }
  },
  typography: {
    useNextVariants: true
  }
});

const useStyles = makeStyles(theme => ({
  formPaper: {
    padding: '20px',
    borderRadius: '8px',
    '&:hover': {
      border: '1px solid #eceff1',
      boxShadow: '0px 5px 8px 6px rgba(0,0,0,0.55)'
    }
  },
  inputPadding: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  buttonPosition: {
    'text-align': 'left',
    marginTop: theme.spacing(4),
    paddingBottom: theme.spacing(1)
  },
  formColor: {
    color: '#b0bec5'
  },
  textFieldLabel: {
    color: `#b0bec5 !important`
  },
  textFieldInput: {
    color: '#fff'
  }
}));

const ContactForm = () => {
  const classes = useStyles();

  return (
    <form name="contact" method="post" className={classes.formPaper}>
      <Title align="center">Get In Touch</Title>
      <MuiThemeProvider theme={inputStyle}>
        <TextField
          id="name"
          label="Name"
          name="name"
          type="text"
          required
          fullWidth
          className={classes.inputPadding}
          inputProps={{
            minLength: 2,
            maxLength: 250
          }}
          // eslint-disable-next-line
          InputProps={{
            classes: {
              root: classes.textFieldInput
            }
          }}
          InputLabelProps={{
            classes: {
              root: classes.textFieldLabel
            }
          }}
        />

        <TextField
          id="email"
          label="Email"
          name="email"
          type="email"
          required
          fullWidth
          className={classes.inputPadding}
          inputProps={{
            minLength: 3,
            maxLength: 120
          }}
          // eslint-disable-next-line
          InputProps={{
            classes: {
              root: classes.textFieldInput
            }
          }}
          InputLabelProps={{
            classes: {
              root: classes.textFieldLabel
            }
          }}
        />
        <TextField
          id="phone"
          label="Phone"
          name="phone"
          type="tel"
          required
          fullWidth
          className={classes.inputPadding}
          inputProps={{
            minLength: 3,
            maxLength: 120
          }}
          // eslint-disable-next-line
          InputProps={{
            classes: {
              root: classes.textFieldInput
            }
          }}
          InputLabelProps={{
            classes: {
              root: classes.textFieldLabel
            }
          }}
        />
        <TextField
          id="message"
          label="Message"
          name="message"
          type="text"
          multiline
          rows="5"
          required
          fullWidth
          InputProps={{
            classes: {
              root: classes.textFieldInput
            }
          }}
          // eslint-disable-next-line
          inputProps={{
            //eslint-disable-line
            minLength: 50,
            maxLength: 1000
          }}
          InputLabelProps={{
            classes: {
              root: classes.textFieldLabel
            }
          }}
        />
      </MuiThemeProvider>
      <Button
        type="submit"
        variant="contained"
        color="primary"
        className={classes.buttonPosition}
      >
        Send message
      </Button>
    </form>
  );
};

ContactForm.propTypes = {};
export default ContactForm;
