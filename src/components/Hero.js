import React from 'react';
import PropTypes from 'prop-types';
import { StaticQuery, graphql } from 'gatsby';
import GatsbyImage from 'gatsby-image';
import useTheme from '@material-ui/core/styles/useTheme';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

import { HeroTitle, HeroSubtitle } from '~components/Typography';

const HeroTextImage = ({ image }) => {
  const theme = useTheme();
  const isSmallerThanMd = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <Grid
      container
      direction={isSmallerThanMd ? 'column-reverse' : 'row'}
      alignItems="center"
      justify="space-between"
      spacing={isSmallerThanMd ? 4 : 0}
    >
      <Grid item xs={12} md={6} lg={5}>
        <HeroTitle>Ex consequat non voluptate incididunt.</HeroTitle>

        <HeroSubtitle>
          Do voluptate reprehenderit tempor sint veniam in aute mollit esse
          eiusmod commodo.
        </HeroSubtitle>

        <Box mt={2}>
          <Button
            component="a"
            href="#about-0"
            variant="contained"
            color="primary"
            size="large"
          >
            Consequat amet
          </Button>
        </Box>
      </Grid>

      <Grid item xs={12} md={6} lg={6} style={{ width: '100%' }}>
        <Box component="div" boxShadow={10}>
          <GatsbyImage
            fluid={image}
            alt="Ad sint nulla quis qui eu."
            title="Consequat culpa cillum voluptate."
          />
        </Box>
      </Grid>
    </Grid>
  );
};

HeroTextImage.propTypes = {
  image: PropTypes.shape().isRequired
};

export default props => (
  <StaticQuery
    query={graphql`
      query {
        file(relativePath: { eq: "hero.jpg" }) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={data => (
      <HeroTextImage image={data.file.childImageSharp.fluid} {...props} />
    )}
  />
);
