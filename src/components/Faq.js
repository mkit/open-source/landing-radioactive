import uuid from 'uuid';
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMoreOutlined';
import Typography from '@material-ui/core/Typography';

import { Title } from '~components/Typography';

const useStyles = makeStyles(theme => ({
  sectionTitle: {
    marginBottom: theme.spacing(10),
    color: '#fff'
  },
  paper: {
    background: '#e0e0e0'
  }
}));

const Faq = ({ faq }) => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title align="center" className={classes.sectionTitle}>
        Често Задавани Въпроси
      </Title>

      {faq &&
        faq.map(qa => (
          <ExpansionPanel key={uuid.v4()} className={classes.paper} square>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography variant="h6" style={{ fontWeight: '300' }}>
                {qa.question}
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>{qa.answer}</Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        ))}
    </React.Fragment>
  );
};

Faq.propTypes = {
  faq: PropTypes.arrayOf(
    PropTypes.shape({
      question: PropTypes.string.isRequired,
      answer: PropTypes.string.isRequired
    })
  ).isRequired
};

export default Faq;
