import React from 'react';
import PropTypes from 'prop-types';
import { Link as MuiLink } from '@material-ui/core';
import { Link as GatsbyLink } from 'gatsby';

const Link = ({ children, to, ...rest }) => {
  const isInternalLink = /^\/(?!\/)/.test(to);

  if (isInternalLink) {
    return (
      <MuiLink component={GatsbyLink} to={to} {...rest}>
        {children}
      </MuiLink>
    );
  }
  return (
    <MuiLink href={to} {...rest}>
      {children}
    </MuiLink>
  );
};

Link.propTypes = {
  children: PropTypes.node.isRequired,
  to: PropTypes.string.isRequired
};

export default Link;
