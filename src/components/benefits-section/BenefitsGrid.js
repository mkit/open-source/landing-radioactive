import React from 'react';
import uuid from 'uuid';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import BenefitsCard from './BenefitsCard';

const BenefitsGrid = ({ benefitsInformation }) => (
  <Grid
    container
    direction="row"
    justify="space-between"
    alignItems="center"
    spacing={4}
  >
    {benefitsInformation &&
      benefitsInformation.map(benefit => (
        <Grid item xs={12} md={4} key={uuid.v4()}>
          <BenefitsCard
            icon={benefit.icon}
            title={benefit.title}
            description={benefit.description}
          />
        </Grid>
      ))}
  </Grid>
);
BenefitsGrid.propTypes = {
  benefitsInformation: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      icon: PropTypes.string.isRequired
    })
  ).isRequired
};
export default BenefitsGrid;
