import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@material-ui/core';

import { Title, Text } from '~components/Typography';

const BenefitsCard = ({ title, description, icon }) => {
  return (
    <React.Fragment>
      <Icon style={{ fontSize: 140 }} color="secondary">
        {icon}
      </Icon>
      <Title>{title}s</Title>
      <Text>{description}</Text>
    </React.Fragment>
  );
};

BenefitsCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
};
export default BenefitsCard;
