import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Typography as MuiTypography } from '@material-ui/core';

const useStyles = makeStyles({
  title: {
    color: '#fff'
  },
  text: {
    color: '#eceff1'
  }
});

export const HeroTitle = ({ children, ...rest }) => {
  const classes = useStyles();
  return (
    <MuiTypography
      className={classes.title}
      variant="h3"
      component="h1"
      gutterBottom
      {...rest}
    >
      {children}
    </MuiTypography>
  );
};

HeroTitle.propTypes = { children: PropTypes.node.isRequired };

export const HeroSubtitle = ({ children, ...rest }) => {
  const classes = useStyles();
  return (
    <MuiTypography
      className={classes.text}
      variant="h5"
      component="h2"
      gutterBottom
      {...rest}
    >
      {children}
    </MuiTypography>
  );
};

HeroSubtitle.propTypes = { children: PropTypes.node.isRequired };

export const Title = ({ children, ...rest }) => {
  const classes = useStyles();
  return (
    <MuiTypography
      className={classes.title}
      variant="h4"
      component="h3"
      gutterBottom
      {...rest}
    >
      {children}
    </MuiTypography>
  );
};

Title.propTypes = { children: PropTypes.node.isRequired };

export const Overline = ({ children, ...rest }) => {
  return (
    <MuiTypography variant="overline" color="secondary" gutterBottom {...rest}>
      {children}
    </MuiTypography>
  );
};

Overline.propTypes = { children: PropTypes.node.isRequired };

export const Text = ({ children, ...rest }) => {
  const classes = useStyles();
  return (
    <MuiTypography
      className={classes.text}
      variant="subtitle1"
      component="p"
      gutterBottom
      {...rest}
    >
      {children}
    </MuiTypography>
  );
};

Text.propTypes = { children: PropTypes.node.isRequired };
