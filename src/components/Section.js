import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import useTheme from '@material-ui/core/styles/useTheme';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

import { Overline, Title, Text } from '~components/Typography';
import Image from '~components/Image';

const useStyles = makeStyles({
  img: {
    display: 'flex',
    width: '100%'
  },
  text: {
    fontSize: '1.1rem',
    lineHeight: '1.6',
    letterSpacing: '0.01em',
    color: '#eceff1'
  },
  textColor: {
    color: '#fff'
  }
});

const Section = ({
  overline,
  title,
  description,
  buttonText,
  imgFilename,
  imgAlt,
  reverse
}) => {
  const theme = useTheme();
  const isSmallerThanMd = useMediaQuery(theme.breakpoints.down('md'));
  const classes = useStyles();

  return (
    <Grid
      container
      direction={
        isSmallerThanMd ? 'column-reverse' : reverse ? 'row-reverse' : 'row' // eslint-disable-line
      }
      alignItems="center"
      justify="space-between"
      spacing={isSmallerThanMd ? 4 : 0}
    >
      <Grid item xs={12} md={6} lg={5}>
        <Overline>{overline}</Overline>

        <Title>{title}</Title>

        <Text>{description}</Text>

        <Box mt={2}>
          <Button
            component="a"
            href="#pricing"
            variant="contained"
            color="primary"
            size="large"
          >
            {buttonText}
          </Button>
        </Box>
      </Grid>

      <Grid item xs={12} md={6} lg={6} style={{ width: '100%' }}>
        <Box
          component={Image}
          filename={imgFilename}
          alt={imgAlt}
          title={title}
          boxShadow={10}
          className={classes.img}
        />
      </Grid>
    </Grid>
  );
};

Section.propTypes = {
  overline: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  imgFilename: PropTypes.string.isRequired,
  imgAlt: PropTypes.string.isRequired,
  reverse: PropTypes.bool
};

Section.defaultProps = {
  reverse: false
};

export default Section;
