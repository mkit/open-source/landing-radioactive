import uuid from 'uuid';
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import useTheme from '@material-ui/styles/useTheme';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import CheckOutlinedIcon from '@material-ui/icons/CheckOutlined';

import { Title } from '~components/Typography';

const useStyles = makeStyles(theme => ({
  paper: {
    backgroundColor: 'unset',
    transform: props =>
      props.isFeatured && !props.isMobile ? 'scale(1.1)' : ''
  },
  sectionTitle: {
    marginBottom: theme.spacing(10),
    color: '#fff'
  },
  planMeta: {
    padding: theme.spacing(2, 3),
    background: props => props.colorAccent,
    color: '#fff'
  },
  planHead: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '180px',
    background: props => props.color,
    '& h5': {
      color: '#eceff1',
      fontWeight: 400,
      '& span': {
        fontSize: '24px',
        fontWeight: 300
      }
    }
  },
  planBody: {
    padding: theme.spacing(2),
    background: theme.palette.background.paper
  },
  hr: {
    margin: 0,
    padding: 0,
    border: '1px solid #e0e0e0'
  },
  planActionArea: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: theme.spacing(2, 4, 2, 4),
    background: theme.palette.background.paper
  },
  text: props => ({
    color: `${props.colorText} !important`
  }),
  listAvatar: {
    color: '#4caf50',
    background: 'unset'
  }
}));

const Plan = ({
  isFeatured,
  overline,
  price,
  title,
  color,
  colorAccent,
  colorText,
  features
}) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('md'));
  const classes = useStyles({
    isFeatured,
    isMobile,
    color,
    colorAccent,
    colorText
  });
  return (
    <Paper elevation={5} className={classes.paper}>
      <Box className={classes.planMeta}>
        <Typography className={classes.text}>{overline}</Typography>
      </Box>
      <Box className={classes.planHead}>
        <Typography
          variant="h2"
          component="h5"
          className={classes.text}
          gutterBottom
        >
          {price}
          <span>лв.</span>
        </Typography>
        <Typography variant="h5" component="h5" className={classes.text}>
          {title}
        </Typography>
      </Box>

      <Box className={classes.planBody}>
        <List dense={false}>
          {features &&
            features.map(feature => (
              <ListItem key={uuid.v4()}>
                <ListItemAvatar>
                  <Avatar className={classes.listAvatar}>
                    <CheckOutlinedIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={feature} />
              </ListItem>
            ))}
        </List>
      </Box>

      <hr className={classes.hr} />

      <Box className={classes.planActionArea}>
        <Button component="a" href="#contacts" color="primary">
          Свържете се с нас
        </Button>
      </Box>
    </Paper>
  );
};

Plan.propTypes = {
  isFeatured: PropTypes.bool,
  overline: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  colorAccent: PropTypes.string.isRequired,
  colorText: PropTypes.string,
  features: PropTypes.arrayOf(PropTypes.string).isRequired
};

Plan.defaultProps = {
  isFeatured: false,
  colorText: '#ffffff'
};

const Pricing = ({ plans }) => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title align="center" className={classes.sectionTitle}>
        Qui laboris consectetur incididunt id sunt.
      </Title>

      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        spacing={4}
      >
        {plans &&
          plans.map(plan => (
            <Grid key={uuid.v4()} item xs={12} md={4}>
              <Plan {...plan} />
            </Grid>
          ))}
      </Grid>
    </React.Fragment>
  );
};

Pricing.propTypes = {
  plans: PropTypes.arrayOf(Plan.propTypes).isRequired
};

export default Pricing;
