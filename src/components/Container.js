import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { makeStyles } from '@material-ui/styles';
import { Container as MuiContainer } from '@material-ui/core';

const useStyles = makeStyles({
  section: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'baseline'
  },
  background: props => ({
    '&::before': {
      position: 'absolute',
      left: '50%',
      right: '50%',
      width: '100vw',
      height: '100%',
      marginLeft: '-50vw',
      marginRight: '-50vw',
      zIndex: '-2',
      content: "''",
      background: props.background,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      filter: props.grayscale ? 'grayscale(100%)' : null
    }
  }),
  overlay: props => ({
    '&::after': {
      position: 'absolute',
      left: '50%',
      right: '50%',
      width: '100vw',
      height: '100%',
      marginLeft: '-50vw',
      marginRight: '-50vw',
      zIndex: '-1',
      content: "''",
      background: props.overlay
    }
  }),
  vh33: {
    minHeight: '33.3vh'
  },
  vh50: {
    minHeight: '50vh'
  },
  vh66: {
    minHeight: '66.6vh'
  },
  vh100: {
    minHeight: '100vh'
  },
  'v-center': {
    justifyContent: 'center'
  },
  'h-center': {
    alignItems: 'center'
  }
});

const Container = ({
  children,
  className,
  size,
  center,
  background,
  overlay,
  grayscale,
  ...rest
}) => {
  const classes = useStyles({ background, overlay, grayscale });

  return (
    <MuiContainer
      className={classNames(
        classes.section,
        // size
        size && size === 33 ? classes.vh33 : '',
        size && size === 50 ? classes.vh50 : '',
        size && size === 66 ? classes.vh66 : '',
        size && size === 100 ? classes.vh100 : '',
        // center
        center && center === 'vertical' ? classes['v-center'] : '',
        center && center === 'horizontal' ? classes['h-center'] : '',
        center && center === true
          ? `${classes['v-center']} ${classes['h-center']}`
          : '',
        // background
        background ? classes.background : '',
        // overlay
        overlay ? classes.overlay : '',
        // include className on component
        className
      )}
      {...rest}
    >
      {children}
    </MuiContainer>
  );
};

Container.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  size: PropTypes.number,
  center: PropTypes.oneOf(['vertical', 'horizontal', true, false]),
  background: PropTypes.string,
  overlay: PropTypes.string,
  grayscale: PropTypes.bool
};
Container.defaultProps = {
  className: undefined,
  size: 0,
  center: false,
  background: '',
  overlay: '',
  grayscale: false
};

export default Container;
