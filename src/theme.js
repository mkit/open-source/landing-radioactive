import { createMuiTheme } from '@material-ui/core/styles';
import { amber, deepPurple, red } from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: deepPurple[900]
    },
    secondary: amber,
    error: red,
    background: {
      default: '#263238'
    }
  }
});

export default theme;
