import React from 'react';
import PropTypes from 'prop-types';
import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';

import '~src/styles.css';
import theme from '~src/theme';

const App = ({ children }) => {
  return (
    <React.Fragment>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </React.Fragment>
  );
};

App.propTypes = {
  children: PropTypes.node.isRequired
};

export default App;
