import uuid from 'uuid';
import React from 'react';
import { Element } from 'react-scroll';

import Page from '~layout/Page';
import SEO from '~layout/SEO';

import Container from '~components/Container';
import Hero from '~components/Hero';
import Section from '~components/Section';
import BenefitsGrid from '~components/benefits-section/BenefitsGrid';
import Pricing from '~components/Pricing';
import ContactForm from '~components/ContactForm';
import Faq from '~components/Faq';

const SECTION_DUMMY_DATA = [
  {
    overline: 'Ex consequat non voluptate incididunt.',
    title: 'Dolore est ad nulla cupidatat ad culpa.',
    description: `Do voluptate reprehenderit tempor sint veniam in aute mollit esse eiusmod commodo. Reprehenderit sint aliquip do laboris non dolor adipisicing labore amet qui. Lorem sunt quis velit sunt exercitation ex. Cillum nisi non ut officia. Dolor ad minim proident aliquip exercitation Lorem anim irure est velit irure dolor. Nostrud irure proident aute minim ullamco Lorem eu magna exercitation eu ea nisi. Duis do occaecat minim incididunt mollit eiusmod ad eu laborum non eiusmod minim. Lorem sint consectetur officia sunt cillum ea elit ut. Aliquip Lorem pariatur labore nulla incididunt.`,
    buttonText: 'Consequat amet',
    imgFilename: 'tower_1.jpg',
    imgAlt: 'Cupidatat commodo duis'
  },
  {
    overline: 'Dolor cupidatat aliquip ullamco.',
    title: 'Elit occaecat quis officia ipsum sit dolor deserunt labore.',
    description: `Veniam velit qui ad in elit tempor consectetur. Voluptate anim ex eu do laboris ullamco do eu deserunt qui est ad pariatur culpa. Irure nisi aliqua ad minim anim consectetur culpa elit amet excepteur. Dolore eu aliquip aute elit qui mollit qui nostrud in dolor culpa labore eu. Lorem aliquip ad ullamco est ut veniam pariatur ad voluptate. Proident officia laborum do ea ullamco. Occaecat culpa non proident enim velit laborum adipisicing in. Culpa veniam nulla quis consectetur cillum irure nisi dolor eu velit commodo proident non in. Voluptate incididunt mollit voluptate eu. Veniam ipsum velit adipisicing cillum id fugiat eu commodo ut.`,
    buttonText: 'Consequat amet',
    imgFilename: 'tower_2.jpg',
    imgAlt: 'Cupidatat commodo duis'
  },
  {
    overline: 'Li amet exercitation cillum ad.',
    title: 'Et tempor minim in enim exercitation sit aliqua nostrud nisi.',
    description: `Anim fugiat eu irure adipisicing commodo ex. Esse sit tempor ex commodo laboris minim. Anim cillum culpa cupidatat veniam in elit commodo cillum adipisicing aliqua amet ipsum sit. Incididunt magna labore sunt veniam quis eu velit. Incididunt commodo sint ea exercitation culpa consectetur laboris nisi irure amet veniam veniam incididunt. Est amet irure esse occaecat veniam voluptate reprehenderit eiusmod. Id magna exercitation ea laboris occaecat ullamco elit adipisicing ea culpa ut velit laborum adipisicing.`,
    buttonText: 'Consequat amet',
    imgFilename: 'tower_3.jpg',
    imgAlt: 'Cupidatat commodo duis'
  }
];

const BENEFITS_DUMMY_DATA = [
  {
    title: 'Dolore est ad nulla.',
    description: `Anim fugiat eu irure adipisicing commodo ex. Esse sit tempor ex commodo laboris minim. Anim cillum culpa cupidatat veniam in elit commodo cillum adipisicing aliqua amet ipsum sit. Incididunt magna labore sunt veniam quis eu velit. Incididunt commodo sint ea exercitation culpa consectetur laboris nisi.`,
    icon: 'accessibility'
  },
  {
    title: 'Dolore est ad nulla.',
    description: `Anim fugiat eu irure adipisicing commodo ex. Esse sit tempor ex commodo laboris minim. Anim cillum culpa cupidatat veniam in elit commodo cillum adipisicing aliqua amet ipsum sit. Incididunt magna labore sunt veniam quis eu velit. Incididunt commodo sint ea exercitation culpa consectetur laboris nisi.`,
    icon: 'accessibility'
  },
  {
    title: 'Dolore est ad nulla.',
    description: `Anim fugiat eu irure adipisicing commodo ex. Esse sit tempor ex commodo laboris minim. Anim cillum culpa cupidatat veniam in elit commodo cillum adipisicing aliqua amet ipsum sit. Incididunt magna labore sunt veniam quis eu velit. Incididunt commodo sint ea exercitation culpa consectetur laboris nisi.`,
    icon: 'accessibility'
  },
  {
    title: 'Dolore est ad nulla.',
    description: `Anim fugiat eu irure adipisicing commodo ex. Esse sit tempor ex commodo laboris minim. Anim cillum culpa cupidatat veniam in elit commodo cillum adipisicing aliqua amet ipsum sit. Incididunt magna labore sunt veniam quis eu velit. Incididunt commodo sint ea exercitation culpa consectetur laboris nisi.`,
    icon: 'accessibility'
  },
  {
    title: 'Dolore est ad nulla.',
    description: `Anim fugiat eu irure adipisicing commodo ex. Esse sit tempor ex commodo laboris minim. Anim cillum culpa cupidatat veniam in elit commodo cillum adipisicing aliqua amet ipsum sit. Incididunt magna labore sunt veniam quis eu velit. Incididunt commodo sint ea exercitation culpa consectetur laboris nisi.`,
    icon: 'accessibility'
  },
  {
    title: 'Dolore est ad nulla.',
    description: `Anim fugiat eu irure adipisicing commodo ex. Esse sit tempor ex commodo laboris minim. Anim cillum culpa cupidatat veniam in elit commodo cillum adipisicing aliqua amet ipsum sit. Incididunt magna labore sunt veniam quis eu velit. Incididunt commodo sint ea exercitation culpa consectetur laboris nisi.`,
    icon: 'accessibility'
  }
];

const PRICING_DUMMY_DATA = [
  {
    overline: 'STARTER',
    price: '49',
    title: 'Getting Started',
    color: '#455a64',
    colorAccent: '#37474f',
    features: [
      'Tempor irure do magna nulla consectetur.',
      'Tempor irure do magna nulla consectetur.',
      'Tempor irure do magna nulla consectetur.'
    ]
  },
  {
    isFeatured: true,
    overline: 'PREMIUM',
    price: '159',
    title: 'Getting Started',
    color: '#ff8f00',
    colorAccent: '#ff6f00',
    colorText: '#000000',
    features: [
      'Tempor irure do magna nulla consectetur.',
      'Tempor irure do magna nulla consectetur.',
      'Tempor irure do magna nulla consectetur.',
      'Tempor irure do magna nulla consectetur.'
    ]
  },
  {
    overline: 'ULTIMATE',
    price: '*',
    title: 'Getting Started',
    color: '#4527a0',
    colorAccent: '#311b92',
    features: [
      'Tempor irure do magna nulla consectetur.',
      'Tempor irure do magna nulla consectetur.',
      'Tempor irure do magna nulla consectetur.'
    ]
  }
];

const FAQ_DUMMY_DATA = [
  {
    question: 'Aute ullamco dolore proident labore mollit fugiat?',
    answer:
      'Aliqua cillum aute culpa veniam pariatur. Irure adipisicing mollit tempor nostrud qui est occaecat pariatur occaecat ea. Dolore voluptate aliquip dolor voluptate minim reprehenderit ea. Laboris ea nostrud exercitation minim reprehenderit enim est ex eu.'
  },
  {
    question: 'Esse id deserunt sint tempor ut ex eiusmod?',
    answer:
      'Aliqua cillum aute culpa veniam pariatur. Irure adipisicing mollit tempor nostrud qui est occaecat pariatur occaecat ea. Dolore voluptate aliquip dolor voluptate minim reprehenderit ea. Laboris ea nostrud exercitation minim reprehenderit enim est ex eu.'
  },
  {
    question: 'Aliqua cupidatat minim qui in pariatur irure deserunt?',
    answer:
      'Aliqua cillum aute culpa veniam pariatur. Irure adipisicing mollit tempor nostrud qui est occaecat pariatur occaecat ea. Dolore voluptate aliquip dolor voluptate minim reprehenderit ea. Laboris ea nostrud exercitation minim reprehenderit enim est ex eu.'
  },
  {
    question: 'Minim irure consequat qui veniam laboris ad?',
    answer:
      'Aliqua cillum aute culpa veniam pariatur. Irure adipisicing mollit tempor nostrud qui est occaecat pariatur occaecat ea. Dolore voluptate aliquip dolor voluptate minim reprehenderit ea. Laboris ea nostrud exercitation minim reprehenderit enim est ex eu.'
  },
  {
    question:
      'Ipsum ex cillum veniam est adipisicing sint occaecat ad irure incididunt?',
    answer:
      'Aliqua cillum aute culpa veniam pariatur. Irure adipisicing mollit tempor nostrud qui est occaecat pariatur occaecat ea. Dolore voluptate aliquip dolor voluptate minim reprehenderit ea. Laboris ea nostrud exercitation minim reprehenderit enim est ex eu.'
  },
  {
    question:
      'Ad fugiat consequat duis in esse sint id consectetur exercitation?',
    answer:
      'Aliqua cillum aute culpa veniam pariatur. Irure adipisicing mollit tempor nostrud qui est occaecat pariatur occaecat ea. Dolore voluptate aliquip dolor voluptate minim reprehenderit ea. Laboris ea nostrud exercitation minim reprehenderit enim est ex eu.'
  },
  {
    question: 'Proident anim laboris commodo mollit amet eu?',
    answer:
      'Aliqua cillum aute culpa veniam pariatur. Irure adipisicing mollit tempor nostrud qui est occaecat pariatur occaecat ea. Dolore voluptate aliquip dolor voluptate minim reprehenderit ea. Laboris ea nostrud exercitation minim reprehenderit enim est ex eu.'
  }
];

const IndexPage = () => (
  <Page>
    <SEO />

    <Container
      size={100}
      center
      background="url(https://images.unsplash.com/photo-1442406964439-e46ab8eff7c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2250&q=80)"
      // overlay="linear-gradient(180deg, #000000 0%, rgba(51, 51, 51, 0.5) 100%);",
      grayscale
    >
      <Hero />
    </Container>

    {SECTION_DUMMY_DATA.map((section, index) => (
      <Element key={uuid.v4()} u name={`about-${index}`}>
        <Container size={66} center>
          <Section {...section} reverse={index % 2 === 0} />
        </Container>
      </Element>
    ))}

    <Element name="pricing">
      <Container size={100} center>
        <Pricing plans={PRICING_DUMMY_DATA} />
      </Container>
    </Element>

    <Container size={100}>
      <BenefitsGrid benefitsInformation={BENEFITS_DUMMY_DATA} />
    </Container>

    <Element name="faq">
      <Container size={66} center>
        <Faq faq={FAQ_DUMMY_DATA} />
      </Container>
    </Element>

    <Element name="contacts">
      <Container size={100} center maxWidth="md">
        <ContactForm />
      </Container>
    </Element>
  </Page>
);

export default IndexPage;
