import React from 'react';
import { Typography, Button } from '@material-ui/core';
import { Link } from 'gatsby';

import Page from '~layout/Page';
import SEO from '~layout/SEO';

import Container from '~components/Container';

const NotFoundPage = () => (
  <Page>
    <SEO />
    <Container size={100} background="#263238" center>
      <Typography
        variant="h1"
        align="center"
        gutterBottom
        style={{ color: '#fff' }}
      >
        404
      </Typography>
      <Typography
        variant="h2"
        align="center"
        gutterBottom
        style={{ color: '#fff' }}
      >
        Page Not Found
      </Typography>
      <Button
        component={Link}
        to="/"
        variant="contained"
        color="primary"
        size="large"
      >
        Начало
      </Button>
    </Container>
  </Page>
);

export default NotFoundPage;
