---
to: src/pages/<%= name %>.js
---
import React from 'react';

import Page from '~layout/Page';
import Seo from '~layout/Seo';
import Container from '~components/Container';

const <%= Name %>Page = () => (
  <Page>
    <Seo />
    <Container>
      <h1>Page <%= Name %></h1>
    </Container>
  </Page>
);

export default <%= Name %>Page;
